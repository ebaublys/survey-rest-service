# Survey REST service

Developed by Edvinas Baublys 11-12-2016

### Introduction

The service uses Spring boot framework. The Maven is used for the project dependencies and build management. The REST service has two API calls /survey/bundle and /survey/products. Each API call has one Http method. One API call has POST method, second API call has PUT method.

In the exercise there was one rule:
>"No persistence is required for this solution(so no JPA/database)"

Therefore I decided to use enums to describe products, questions and bundles. The project consists of 3 modules:

  - application
  - business
  - model

**application** module contains Main application class, as well as other application scope classes like a controllers. Also this module is main module and has application.properties file and spring-boot-starter-web dependency.

**business** module contains all classes witch is responsible for business logic. Usually the classes may be called "service classes".

**model** module contains all database logic. If there the database is used, repositories classes would be hold in this module. The enums also is located at this module.

### API calls documentation

There is two API calls in this service. All calls is described below:

**1 API call**
***
**Title:** Show proper Bundle
***
**URL:** /survey/bundle
***
**Method:** POST
***
**URL Params:** Not specified
***
**Data Params:**
```json
{
    "age":[string constant],
    "student":[boolean],
    "income":[string constant]
}
```
*Example:*
```json
{
    "age": "ADULT",
    "student": true,
    "income": "HIGH"
}
```
***
**Success Response:**

* Code: 201 Created
* Content:
```json
    {
  "title": "Gold",
  "products": [
    {
      "id": 2,
      "title": "Current Account Plus",
      "type": "Account",
      "rule": "RULE_5"
    },
    {
      "id": 6,
      "title": "Debit Card",
      "type": "Card",
      "rule": "RULE_8"
    },
    {
      "id": 8,
      "title": "Gold Credit Card",
      "type": "Card",
      "rule": "RULE_5"
    }
  ],
  "id": 5
}
```
***
**Error Response:**

* Code: 422 Unprocessable Entity
* Content:
```json
{
  "error": "422",
  "message": "No proper Bundle found by represented answers"
}
```
or

* Code: 404 Not Found
* Content:
```json
{
  "error": "404",
  "message": "It seems that you try to send an empty json object"
}
```
***
**header:** Content-Type: application/json
***

**2 API call**
***
**Title:** Update Bundle of selected products
***
**URL:** /survey/products
***
**Method:** PUT
***
**URL Params:** Not specified
***
**Data Params:**
```json
{
"answers" : {
    "age" : [string constant],
    "student" : [boolean],
    "income" : [string constant]
    },

"products":
  [
    {"product": [string constant]},
    {"product": [string constant]},
    {"product": [string constant]},
    {"product": [string constant]}
    ]
}
```
*Example:*
```json
{
"answers" : {
    "age" : "ADULT",
    "student" : false,
    "income" : "HIGH"
    },

"products":
  [
    {"product": "CURRENT_ACCOUNT"},
    {"product": "DEBIT_CARD"},
    {"product": "CREDIT_CARD"},
    {"product": "GOLD_CREDIT_CARD"}
    ]
}
```
***
**Success Response:**

* Code: 200 Ok
* Content: nothing returns

***
**Error Response:**

* Code: 422 Unprocessable Entity
* Content:
```json
{
  "error": "422",
  "message": "No proper Products found by represented answers"
}
```
or

* Code: 404 Not Found
* Content:
```json
{
  "error": "404",
  "message": "It seems that you try to send an empty json object"
}
```
or

* Code: 422 Unprocessable Entity
* Content:
```json
{
  "error": "422",
  "message": "Only one account can be selected"
}
```
or

* Code: 422 Unprocessable Entity
* Content:
```json
{
  "error": "422",
  "message": "There is product found witch not mach represented answers"
}
```
***
**header:** Content-Type: application/json
***

### Available constants

* age: TEEN, ADULT, OLD
* income: NO, LOW, AVERAGE, HIGH
* products: CURRENT_ACCOUNT, CURRENT_ACCOUNT_PLUS, JUNIOR_SAVER_ACCOUNT, STUDENT_ACCOUNT, PENSIONER_ACCOUNT, DEBIT_CARD, CREDIT_CARD, GOLD_CREDIT_CARD

### Tests

There are 6 UnitTests created.
* **business** module has 2 tests (tests verify few methods business logic)
* **application** module has 4 tests (tests verify HTTP scope rules: expected status code, errors, etc.)

### Installation

Clone the code from BitBucket Url: https://ebaublys@bitbucket.org/ebaublys/survey-rest-service.git

The lines below run all 6 tests, install packages, build, and run service.
First go to the root application directory where the main pom.xml is located. Then use these commands.

```sh
$ mvn clean install
$ cd application
$ mvn spring-boot:run
```

### Usage

You can use any of Http client to test "Survey REST service" API calls.
