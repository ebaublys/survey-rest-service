package com.seb.webservices.model.dto;

import com.seb.webservices.constants.questions.Age;
import com.seb.webservices.constants.questions.Income;

/**
 * Created by Edvis on 2016-11-10.
 */
public class SelectedAnswers {

    private Age age;
    private boolean student;
    private Income income;

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public boolean isStudent() {
        return student;
    }

    public void setStudent(boolean student) {
        this.student = student;
    }

    public Income getIncome() {
        return income;
    }

    public void setIncome(Income income) {
        this.income = income;
    }
}
