package com.seb.webservices.model.dto;

import com.seb.webservices.constants.products.Product;

/**
 * Created by Edvis on 2016-11-12.
 */

public class SelectedProduct {

    private Product product;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
