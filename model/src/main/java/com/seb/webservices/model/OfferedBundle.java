package com.seb.webservices.model;

import com.seb.webservices.constants.products.Product;

/**
 * Created by Edvis on 2016-11-10.
 */
public class OfferedBundle {

    private int Id;

    private String title;

    private Product[] products;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }
}
