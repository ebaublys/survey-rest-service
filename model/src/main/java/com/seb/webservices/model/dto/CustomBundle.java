package com.seb.webservices.model.dto;

import java.util.List;

/**
 * Created by Edvis on 2016-11-12.
 */
public class CustomBundle {

    private SelectedAnswers answers;
    private List<SelectedProduct> products;

    public SelectedAnswers getAnswers() {
        return answers;
    }

    public void setAnswers(SelectedAnswers answers) {
        this.answers = answers;
    }

    public List<SelectedProduct> getProducts() {
        return products;
    }

    public void setProducts(List<SelectedProduct> products) {
        this.products = products;
    }
}
