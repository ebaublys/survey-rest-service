package com.seb.webservices.constants.questions;

/**
 * Created by Edvis on 2016-11-10.
 */

public enum Income {

    NO(0, 0),
    LOW(1, 12000),
    AVERAGE(12001, 40000),
    HIGH(40001, 40001);

    private final int rangeStart;
    private final int rangeEnd;

    Income(int rangeStart, int rangeEnd) {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }

}
