package com.seb.webservices.constants.errors;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Created by Edvis on 2016-11-11.
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MyCustomResponseError {

    NO_PROPER_BUNDLE_ERROR("422", "No proper Bundle found by represented answers"),
    NO_PROPER_PRODUCT_ERROR("422", "No proper Products found by represented answers"),
    BAD_PRODUCT_SELECTED_ERROR("422", "There is product found witch not mach represented answers"),
    MULTIPLE_ACCOUNT_SELECTED_ERROR("422", "Only one account can be selected"),
    NULL_POINTER_ERROR("404", "It seems that you try to send an empty json object");

    private final String error;
    private final String message;

    MyCustomResponseError(final String error, final String message){
        this.error = error;
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
