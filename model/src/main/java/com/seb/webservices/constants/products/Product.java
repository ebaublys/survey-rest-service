package com.seb.webservices.constants.products;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.seb.webservices.constants.rules.Rule;

/**
 * Created by Edvis on 2016-11-11.
 */

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Product {

    CURRENT_ACCOUNT(1, "Current Account", "Account", Rule.RULE_6),
    CURRENT_ACCOUNT_PLUS(2, "Current Account Plus", "Account", Rule.RULE_5),
    JUNIOR_SAVER_ACCOUNT(3, "Junior Saver Account", "Account", Rule.RULE_1),
    STUDENT_ACCOUNT(4, "Student Account", "Account", Rule.RULE_2),
    PENSIONER_ACCOUNT(5, "Pensioner Account", "Account", Rule.RULE_7),
    DEBIT_CARD(6, "Debit Card", "Card", Rule.RULE_8),
    CREDIT_CARD(7, "Credit Card", "Card", Rule.RULE_4),
    GOLD_CREDIT_CARD(8, "Gold Credit Card", "Card", Rule.RULE_5);

    private final int id;
    private final String title;
    private final String type;
    private final Rule rule;

    private Product(final int id, final String title, final String type, final Rule rule){
        this.id = id;
        this.title = title;
        this.type = type;
        this.rule = rule;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public Rule getRule() {
        return rule;
    }
}
