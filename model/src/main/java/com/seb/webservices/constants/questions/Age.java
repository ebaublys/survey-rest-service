package com.seb.webservices.constants.questions;

/**
 * Created by Edvis on 2016-11-10.
 */

public enum Age {

    TEEN(0,17),
    ADULT(18, 64),
    OLD(65, 65);

    private final int rangeStart;
    private final int rangeEnd;

    Age(int rangeStart, int rangeEnd) {
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
    }

    public int getRangeStart() {
        return rangeStart;
    }

    public int getRangeEnd() {
        return rangeEnd;
    }
}
