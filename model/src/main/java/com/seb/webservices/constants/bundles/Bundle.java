package com.seb.webservices.constants.bundles;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.seb.webservices.constants.products.Product;
import com.seb.webservices.constants.rules.Rule;

/**
 * Created by Edvis on 2016-11-11.
 */


@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Bundle {

    JUNIOR_SAVER(1, "Junior Saver", new Product[]{Product.JUNIOR_SAVER_ACCOUNT}, Rule.RULE_1, 0),
    STUDENT(2, "Student", new Product[]{Product.STUDENT_ACCOUNT, Product.DEBIT_CARD, Product.CREDIT_CARD}, Rule.RULE_2, 0),
    CLASSIC(3, "Classic", new Product[]{Product.CURRENT_ACCOUNT, Product.DEBIT_CARD}, Rule.RULE_3, 1),
    CLASSIC_PLUS(4, "Classic Plus", new Product[]{Product.CURRENT_ACCOUNT, Product.DEBIT_CARD, Product.CREDIT_CARD}, Rule.RULE_4, 2),
    GOLD(5, "Gold", new Product[]{Product.CURRENT_ACCOUNT_PLUS, Product.DEBIT_CARD, Product.GOLD_CREDIT_CARD}, Rule.RULE_5, 3);

    private final int id;
    private final String title;
    private final Product[] products;
    private final Rule rule;
    private final int value;

    private Bundle(final int id, final String title, final Product[] products, final Rule rule, final int value){
        this.id = id;
        this.title = title;
        this.products = products;
        this.rule = rule;
        this.value = value;

    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Product[] getProducts() {
        return products;
    }

    public Rule getRule() {
        return rule;
    }

    public int getValue() {
        return value;
    }
}
