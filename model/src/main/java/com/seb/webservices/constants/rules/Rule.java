package com.seb.webservices.constants.rules;

/**
 * Created by Edvis on 2016-11-11.
 */
public enum Rule {

    RULE_1(),
    RULE_2(),
    RULE_3(),
    RULE_4(),
    RULE_5(),
    RULE_6(),
    RULE_7(),
    RULE_8();
}
