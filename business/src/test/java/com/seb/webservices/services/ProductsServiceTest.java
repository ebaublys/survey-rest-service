package com.seb.webservices.services;

import com.seb.webservices.constants.bundles.Bundle;
import com.seb.webservices.constants.products.Product;
import com.seb.webservices.model.dto.SelectedProduct;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edvis on 2016-11-12.
 */

public class ProductsServiceTest{

    private ProductsService productsService;

    @Before
    public void setup() {
        this.productsService = new ProductsService();
    }

    @Test
    public void checkAnExpectedBundle(){

        List<Bundle> bundles = buildListOfBundles(new Bundle[]{
                Bundle.CLASSIC,
                Bundle.CLASSIC_PLUS,
                Bundle.GOLD,
                Bundle.JUNIOR_SAVER,
                Bundle.STUDENT});

        Bundle offeredBundle = productsService.getHighestBundle(bundles);

        Assert.assertEquals(5, offeredBundle.getId());
    }

    @Test
    public void checkIfCustomBundleContainsMultipleAccounts(){

        List<SelectedProduct> products = buildListOfProducts(new Product[]{
                Product.CURRENT_ACCOUNT,
                Product.JUNIOR_SAVER_ACCOUNT,
                Product.PENSIONER_ACCOUNT,
                Product.DEBIT_CARD});

        boolean contains = productsService.checkIfMultipleAccount(products);

        Assert.assertEquals(true, contains);

    }

    private List<SelectedProduct> buildListOfProducts(Product[] products){
        List<SelectedProduct> preparedList = new ArrayList<>();

        for (int i=0; i<products.length; i++){
            SelectedProduct product = new SelectedProduct();
            product.setProduct(products[i]);
            preparedList.add(product);
        }
        return preparedList;
    }

    private List<Bundle> buildListOfBundles(Bundle[] bundles){

        List<Bundle> preparedBundles = new ArrayList<>();

        for (int i=0; i<bundles.length; i++){
            preparedBundles.add(bundles[i]);
        }
        return preparedBundles;
    }

}
