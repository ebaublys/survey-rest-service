package com.seb.webservices.utils;

import com.seb.webservices.constants.questions.Age;
import com.seb.webservices.constants.questions.Income;
import com.seb.webservices.constants.rules.Rule;
import com.seb.webservices.model.dto.SelectedAnswers;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edvis on 2016-11-11.
 */

@Service
public class RulesDefinition {

    public List<Rule> getListOfRules(SelectedAnswers selectedAnswers){

        Age age = selectedAnswers.getAge();
        boolean student = selectedAnswers.isStudent();
        Income income = selectedAnswers.getIncome();

        List<Rule> rules = new ArrayList<Rule>();

        //Rule 1 definition
        if(age.equals(Age.TEEN)){
            rules.add(Rule.RULE_1);
        }

        //Rule 2 definition
        if((age.equals(Age.ADULT) || age.equals(Age.OLD)) && student){
            rules.add(Rule.RULE_2);
        }

        //Rule 3 definition
        if((age.equals(Age.ADULT) || age.equals(Age.OLD)) && (income.equals(Income.LOW) || income.equals(Income.AVERAGE) || income.equals(Income.HIGH))){
            rules.add(Rule.RULE_3);
        }

        //Rule 4 definition
        if((income.equals(Income.AVERAGE) || income.equals(Income.HIGH)) && (age.equals(Age.ADULT) || age.equals(Age.OLD))){
            rules.add(Rule.RULE_4);
        }

        //Rule 5 definition
        if(income.equals(Income.HIGH) && (age.equals(Age.ADULT) || age.equals(Age.OLD))){
            rules.add(Rule.RULE_5);
        }

        //Rule 6 definition
        if((income.equals(Income.LOW) || income.equals(Income.AVERAGE) || income.equals(Income.HIGH)) && (age.equals(Age.ADULT) || age.equals(Age.OLD))){
            rules.add(Rule.RULE_6);
        }

        //Rule 7 definition
        if(age.equals(Age.OLD)){
            rules.add(Rule.RULE_7);
        }

        //Rule 8 definition
        if (rules.contains(Rule.RULE_2) || rules.contains(Rule.RULE_5) || rules.contains(Rule.RULE_6) || rules.contains(Rule.RULE_7)){
            rules.add(Rule.RULE_8);
        }

        return rules;
    }
}
