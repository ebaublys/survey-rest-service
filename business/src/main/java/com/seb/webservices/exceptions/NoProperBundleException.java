package com.seb.webservices.exceptions;


/**
 * Created by Edvis on 2016-11-11.
 */

public class NoProperBundleException extends Exception {

    public NoProperBundleException() {
    }

    public NoProperBundleException(String message) {
        super(message);
    }
}
