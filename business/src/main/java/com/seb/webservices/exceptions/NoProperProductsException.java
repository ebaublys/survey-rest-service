package com.seb.webservices.exceptions;


/**
 * Created by Edvis on 2016-11-11.
 */

public class NoProperProductsException extends Exception {

    public NoProperProductsException() {
    }

    public NoProperProductsException(String message) {
        super(message);
    }
}
