package com.seb.webservices.exceptions;


/**
 * Created by Edvis on 2016-11-11.
 */

public class MultipleAccountsException extends Exception {

    public MultipleAccountsException() {
    }

    public MultipleAccountsException(String message) {
        super(message);
    }
}
