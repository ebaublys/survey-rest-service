package com.seb.webservices.exceptions;


/**
 * Created by Edvis on 2016-11-11.
 */

public class BadProductsSelectException extends Exception {

    public BadProductsSelectException() {
    }

    public BadProductsSelectException(String message) {
        super(message);
    }
}
