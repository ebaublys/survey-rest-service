package com.seb.webservices.services;

import com.seb.webservices.constants.bundles.Bundle;
import com.seb.webservices.constants.rules.Rule;
import com.seb.webservices.exceptions.BadProductsSelectException;
import com.seb.webservices.exceptions.MultipleAccountsException;
import com.seb.webservices.exceptions.NoProperBundleException;
import com.seb.webservices.exceptions.NoProperProductsException;
import com.seb.webservices.model.OfferedBundle;
import com.seb.webservices.model.dto.CustomBundle;
import com.seb.webservices.model.dto.SelectedAnswers;
import com.seb.webservices.model.dto.SelectedProduct;
import com.seb.webservices.utils.RulesDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Edvis on 2016-11-10.
 */

@Service
public class ProductsService {

    @Autowired
    RulesDefinition rulesDefinition;

    private final static Logger LOGGER = Logger.getLogger(ProductsService.class.getName());

    //Returns suitable Bundle
    public OfferedBundle getSuitableBundle(SelectedAnswers selectedAnswers) throws Exception {

        //Check if passing an empty json object
        if (selectedAnswers == null)

            throw new NullPointerException();

        //Gets list of proper rules by the questions
        List<Rule> pickedRules = rulesDefinition.getListOfRules(selectedAnswers);

        //Check if there are no suitable bundle
        if (pickedRules.isEmpty())

            throw new NoProperBundleException();

        //Collects list of suitable Bundles
        List<Bundle> pickedBundles = getBundleByRules(pickedRules);

        //Check if at least one Bundle was found
        if (pickedBundles.isEmpty())

            throw new NoProperBundleException();

        //Pick highest level of bundle
        Bundle pickedBundle = getHighestBundle(pickedBundles);

        //There is building the new response object instance
        OfferedBundle ob = new OfferedBundle();
        ob.setId(pickedBundle.getId());
        ob.setTitle(pickedBundle.getTitle());
        ob.setProducts(pickedBundle.getProducts());

        LOGGER.info("Successfully found Bundle: " + ob.getTitle());
        return ob;
    }

    //Updates custom Bundle if meet the rules
    public void updateBundle(CustomBundle customBundle) throws Exception {

        //Check if products list is empty
        if (customBundle.getAnswers() == null || customBundle.getProducts() == null || customBundle.getProducts().isEmpty())

            throw new NullPointerException();

        //Gets list of proper rules by the questions
        List<Rule> pickedRules = rulesDefinition.getListOfRules(customBundle.getAnswers());

        //Check if there are no suitable products
        if (pickedRules.isEmpty())

            throw new NoProperProductsException();

        //Check if there are not suitable products selected

        if (checkIfBadProductsFound(customBundle.getProducts(), pickedRules))

            throw new BadProductsSelectException();

        //Check if there more than one Account selected
        if (checkIfMultipleAccount(customBundle.getProducts()))

            throw new MultipleAccountsException();

        LOGGER.info("The custom Bundle was successfully updated. There was " + customBundle.getProducts().size() + " products selected");
    }

    //This method finds bundle with highest value and returns it
    public Bundle getHighestBundle(List<Bundle> bundles) {

        Bundle pickedBundle = bundles.get(0);
        int value = 0;

        for (Bundle b : bundles) {
            if (b.getValue() >= value) {
                pickedBundle = b;
                value = b.getValue();
            }
        }
        return pickedBundle;
    }

    //This method defines if there are more than one Account selected
    public boolean checkIfMultipleAccount(List<SelectedProduct> selectedProducts) {

        int counter = 0;

        for (SelectedProduct s : selectedProducts) {

            if (s.getProduct().getType().equals("Account"))
                counter++;

        }

        if (counter > 1)

            return true;

        return false;

    }

    //This method defines if there are products that does not meet the rules by the questions
    public boolean checkIfBadProductsFound(List<SelectedProduct> userSelectedProducts, List<Rule> pickedRules) {

        for (SelectedProduct s : userSelectedProducts) {

            boolean trigger = true;

            for (Rule r : pickedRules) {

                if (s.getProduct().getRule().equals(r))
                    trigger = false;

            }

            if (trigger)

                return true;
        }

        return false;
    }

    //This method collects list of suitable bundle
    public List<Bundle> getBundleByRules(List<Rule> pickedRules) {

        List<Bundle> pickedBundles = new ArrayList<Bundle>();

        for (Rule r : pickedRules) {
            for (Bundle b : Bundle.values()) {
                if (r.equals(b.getRule())) {
                    pickedBundles.add(b);
                }
            }
        }
        return pickedBundles;
    }
}


