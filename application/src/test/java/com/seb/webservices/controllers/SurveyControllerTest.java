package com.seb.webservices.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.webservices.Main;
import com.seb.webservices.constants.errors.MyCustomResponseError;
import com.seb.webservices.constants.products.Product;
import com.seb.webservices.model.OfferedBundle;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Edvis on 2016-11-12.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebIntegrationTest
public class SurveyControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setup(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void ifUserIsTeenReturnCorrectJsonDataAndStatusCode() throws Exception {

        String bundleUrl = "/survey/bundle";
        ObjectMapper mapper = new ObjectMapper();

        JSONObject request = new JSONObject();
        request.put("age", "TEEN");
        request.put("student", true);
        request.put("income", "LOW");

        OfferedBundle expectedBundle = new OfferedBundle();
        expectedBundle.setId(1);
        expectedBundle.setTitle("Junior Saver");
        expectedBundle.setProducts(new Product[]{Product.JUNIOR_SAVER_ACCOUNT});

        String expectedJson = mapper.writeValueAsString(expectedBundle);

        mockMvc.perform(post(bundleUrl).contentType(MediaType.APPLICATION_JSON_VALUE).content(request.toString()))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedJson, false))
                .andDo(print())
                .andReturn();

    }

    @Test
    public void ifSuitableBundleIsNotFoundReturnRightErrorMessage() throws Exception {

        String bundleUrl = "/survey/bundle";
        ObjectMapper mapper = new ObjectMapper();

        JSONObject request = new JSONObject();
        request.put("age", "ADULT");
        request.put("student", false);
        request.put("income", "NO");

        MyCustomResponseError expectedError = MyCustomResponseError.NO_PROPER_BUNDLE_ERROR;

        String expectedJson = mapper.writeValueAsString(expectedError);

        mockMvc.perform(post(bundleUrl).contentType(MediaType.APPLICATION_JSON_VALUE).content(request.toString()))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedJson, false))
                .andDo(print())
                .andReturn();

    }

    @Test
    public void ifUserSelectsMultipleAccountsReturnError() throws Exception {

        String bundleUrl = "/survey/products";
        ObjectMapper mapper = new ObjectMapper();

        String requestJson = buildRequestJson(
                "ADULT",
                true,
                "AVERAGE",
                new String[]{
                        "CURRENT_ACCOUNT",
                        "STUDENT_ACCOUNT",
                        "CREDIT_CARD"});

        String expectedJson = mapper.writeValueAsString(MyCustomResponseError.MULTIPLE_ACCOUNT_SELECTED_ERROR);

        mockMvc.perform(put(bundleUrl).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedJson, false))
                .andDo(print())
                .andReturn();

    }

    @Test
    public void ifUserSelectsNotRightProductsReturnError() throws Exception {

        String bundleUrl = "/survey/products";
        ObjectMapper mapper = new ObjectMapper();

        String requestJson = buildRequestJson(
                "TEEN",
                true,
                "AVERAGE",
                new String[]{
                        "CURRENT_ACCOUNT",
                        "STUDENT_ACCOUNT",
                        "CREDIT_CARD"});

        String expectedJson = mapper.writeValueAsString(MyCustomResponseError.BAD_PRODUCT_SELECTED_ERROR);

        mockMvc.perform(put(bundleUrl).contentType(MediaType.APPLICATION_JSON_VALUE).content(requestJson))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(expectedJson, false))
                .andDo(print())
                .andReturn();

    }


    private String buildRequestJson(String age, boolean student, String income, String[] products){

        JSONObject answers = new JSONObject();
        answers.put("age", age);
        answers.put("student", student);
        answers.put("income", income);

        JSONArray productsList = new JSONArray();
        for (int i = 0; i < products.length; i++){
            JSONObject product = new JSONObject();
            product.put("product", products[i]);
            productsList.put(product);
        }

        JSONObject j = new JSONObject();
        j.put("answers", answers);
        j.put("products", productsList);

        return j.toString();

    }

}
