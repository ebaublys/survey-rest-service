package com.seb.webservices.controllers;

import com.seb.webservices.constants.errors.MyCustomResponseError;
import com.seb.webservices.exceptions.BadProductsSelectException;
import com.seb.webservices.exceptions.MultipleAccountsException;
import com.seb.webservices.exceptions.NoProperBundleException;
import com.seb.webservices.exceptions.NoProperProductsException;
import com.seb.webservices.model.dto.CustomBundle;
import com.seb.webservices.services.ProductsService;
import com.seb.webservices.model.OfferedBundle;
import com.seb.webservices.model.dto.SelectedAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


/**
 * Created by Edvis on 2016-11-10.
 */

@RestController
@RequestMapping("/survey")
public class SurveyController {

    @Autowired
    private ProductsService productsService;

    @ResponseStatus(code = HttpStatus.CREATED)
    @RequestMapping(value="/bundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public OfferedBundle getBundle(@RequestBody SelectedAnswers selectedAnswers) throws Exception{

        return productsService.getSuitableBundle(selectedAnswers);

    }

    @ResponseStatus(code = HttpStatus.OK)
    @RequestMapping(value="/products", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateProducts(@RequestBody CustomBundle customBundle) throws Exception{

        productsService.updateBundle(customBundle);

    }

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = NoProperBundleException.class)
    public MyCustomResponseError handleNoProperBundleException(Exception e){
        return MyCustomResponseError.NO_PROPER_BUNDLE_ERROR;
    }

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = BadProductsSelectException.class)
    public MyCustomResponseError handleBadProductsSelectException(Exception e){
        return MyCustomResponseError.BAD_PRODUCT_SELECTED_ERROR;
    }

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = MultipleAccountsException.class)
    public MyCustomResponseError handleMultipleAccountsException(Exception e){
        return MyCustomResponseError.MULTIPLE_ACCOUNT_SELECTED_ERROR;
    }

    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = NoProperProductsException.class)
    public MyCustomResponseError handleNoProperProductsException(Exception e){
        return MyCustomResponseError.NO_PROPER_PRODUCT_ERROR;
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = NullPointerException.class)
    public MyCustomResponseError handleNullPointerException(Exception e){
        return MyCustomResponseError.NULL_POINTER_ERROR;
    }
}
