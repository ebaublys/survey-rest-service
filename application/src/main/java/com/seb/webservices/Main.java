package com.seb.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Edvis on 2016-11-10.
 */

@SpringBootApplication
public class Main {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(Main.class, args);
    }

}
